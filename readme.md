# Angular and Aurelia: Descendants of Angular 1

Today new SPA frameworks appear almost every day, and today we'll be comparing two of them: Angular and Aurelia. Why? They were developed and released at approximately  the same time, they have similar philosophy, they are direct competitors and both of them are descendants of good old Angular 1. 

Long story short, Aurelia was created by Rob Eisenberg, known as the creator of Durandal and Caliburn. He worked at Google in the Angular 2 team and left it in 2014 after his views of how a modern framework should look like and Google's plans for Angular went to opposite directions.    

Components (or Custom Elements) associated with its templates are the core of both Angular and Aurelia apps. You're required to have a root component (i.e. app). Both Angular and Aurelia heavily use decorators for component configuration. Each component has a fixed lifecycle that we can hook into.

## Aurelia vs. Angular: A Code Comparison
 
Repos with code: 
[Angular](https://github.com/strrife/angular2-seed), 
[Aurelia](https://github.com/strrife/aurelia-skeleton-esnext).
  
One of the main factors choosing an SPA-framework is the community and ecosystem.  
Both Angular and Aurelia obviously have basic things (router, template engine, validation, etc), it's also not a problem to get a native modal or use some third-part library.
But it comes as no surprise that Angular has a bigger community and bigger development team. 
Angular is in a single github repo which has roughly 21K stars whilst Aurelia main repo has 9K and the the aggregated number of starts somewhat exceeds 12K. 
On StackOverflow the gap gets even bigger: 39K questions tagged `angular2` and only 2K tagged `aurelia`, though here I must say than when I started with Aurelia almost a year and a half ago hen I need some help the community and the core team was super helpful on StackOverflow. 

Both frameworks are open-source: Angular is mainly developed by Google and is not intended to be commercialized whilst Durandal, Inc., Rob Eisenberg's company that employs core team, is following Ember's model of monetization via consulting and training. 

## The key

The Key difference, according to Rob Eisenberg, is in the code: Aurelia is unobtrusive. It's convention over configuration, and 95% of times you'd be just fine with default conventions i.e. template naming, element naming, etc., while Angular requires you to provide configs for basically everything.

When working with Aurelia, after you set up the router and do basic configs it's not likely you'll feel you're working on an Aurelia app anymore. Of course, there are some template-binding guidelines, etc, but when developing an Aurelia app, you write in ES6 or Typescript, and the templates look like absolutely sane HTML, especially when comparing to Angular, as you'll see below.
   
## I want to see the code   

In this overview we'll see some of the major, most notable or representative features that would help understand the philosophy behind the frameworks.

After cloning seed projects for [Angular](https://github.com/angular/angular2-seed) and [Aurelia](https://github.com/aurelia/skeleton-navigation) we get a ES6 Aurelia app (you can use Jspm/System.js, Webpack and RequireJs in combination with ES6 or Typescript) and a TypeScript Angular app (WebPack).    

Let's roll.

## Binding

Angular 1 used dirty-checking for everything which caused lots of performance issues. 
Neither Angular 2 nor Aurelia followed the same path.

In Angular you bind with square brackets and use parenthesis to "bind" to events. 

```
<element [property]="value"></a>
```

Having that said, two-way binding is a combination of both. 

So, for an input this would work just fine:

```html
<input type="text" [(ngModel)]="text">
{{text}} 
```
 
In other words, parenthesis represent and square brackets represent *pushing* value to input.

Angular team did a great job separating binding directions: to the DOM, from the DOM, both directions. Also there's a lot of [syntax sugar](https://angular.io/docs/ts/latest/cheatsheet.html) related to binding classes and styles:

```html
<div [class.red-container]="isRed"></div>	
<div [style.width.px]="elementWidth"></div>	

```     

But what if we want to bind something two-way into a Component?
   
### Two-way binding into component
   
```html
<!-- parent component -->
<input type="text" [(ngModel)]="text">
{{ text }}

<my-component [(text)]="text"></my-component>
```
  
```typescript
import {Component, Input} from '@angular/core';

@Component(/* ... */)
export class MyComponent {
  @Input() text : string;
}
```  

```html
<!-- child component -->
<input [(ngModel)]="text">
Text in child: {{ text }}
```

Now we have something interesting. Updating value in parent input changes values everywhere but modifying child's input only affects the child, i.e. "Text in child" changes, but parent's `text` is not updated.
We need to emit an event informing the parent. The naming rule is property name + 'Change':  
  
```typescript
import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component(/* ... */)
export class MyComponent {
  @Input() text : string;
  @Output() textChange = new EventEmitter();

  triggerUpdate() {
    this.textChange.emit(this.text);
  }
}
```  
The two-way binding starts working properly right after we bind to `ngModelChange` event:
```html
<!-- child component -->
<input [(ngModel)]="text" (ngModelChange)="triggerUpdate($event)">
```

### One-time binding

In Angular 1 we used `{{::value}}` to bind one time.

In Angular 2, things get complicated. Documentations says that you can use `changeDetection: ChangeDetectionStrategy.OnPush` attribute in Component config, but that will make all your binding one-time. 

### Aurelia way

In Aurelia thins are really simple. You can use either interpolation just like Angular `property="${value}"` or one of the following binding types:

* `property.one-time="value"`
* `property.one-way="value"`
* `property.two-way="value"`

The names are self-explanatory. `property.bind="value"` is basically a syntax-sugar self-detecting whether the binding should be one-way or two-way (for instance, for inputs). 

```
<!-- parent-->
<template bindable="text">
  <input type="text" value.bind="text"/>
  <child text.two-way="text"></child>
</template>

<!-- child custom element -->
<template bindable="text">
  <input type="text" value.bind="text"/>
</template>

```

Both `@bindable` and `@Input` are configurable, i.e. you can change name of the property being bound, etc. 

To bind to events in Aurelia you use `.trigger` and `.delegate`. For example, if child component fires an event, maybe custom:

```
// child.js
this.element.dispatchEvent(new CustomEvent('change', {
   detail: someDetails
}));
```

To listen to that in parent you either do

```html
<child change.trigger="myChangeHandler($event)"></child>
<!-- or -->
<child change.delegate="myChangeHandler($event)"></child>
```
  
The difference between those two is that `.trigger` creates an avent handler on that particular element whilst `.delegate` adds listener on `document`. That saves resources but it obviously won't work for non-bubbling events. 










## Basic Component

Let's create basic component rendering an SVG. It'll be awesome, therefpre we'll call it `awesome-svg`. This will illustrate both basic functionality and philosophy.

We'll start with Aurelia. 

```javascript 
// awesome-svg.js

import {bindable} from 'aurelia-framework';

export class AwesomeSvgCustomElement {
  @bindable title;
  @bindable colors = [];
}

```

In Aurelia you can specify the template (or use inline one) with `@template`, `@inlineView` or even `@noView` annotation, but out of the box it searches for the `.html` file with the same as the `.js` file. 
Same for the custom element's name: you can set it with `@customElement('awesome-svg')`. If you don't, Aurelia will convert the title to dash-case and you'll be good. 

Since we didn't specify any of those, the element will be called `awesome-svg` and Aurelia will search for the template named with the same name as the js file (i.e. `awesome-svg.html`) in the same directory as the js file:

```html
<!-- awesome-svg.html -->

<template>
  <h1>${title}</h1>
  <svg>
    <rect repeat.for="color of colors" 
          fill.bind="color" 
          x.bind="$index * 100" y="0" 
          width="50" height="50"></rect>
  </svg>
</template>
```

Notice that `<template>` tag? It's required to wrap all templates into `<template>`.

One more thing is worth noticing: `for ... of` and string interpolation `${title}`: just like you do that in ES6.
   
Now we ant to use the component. To do that we should either import that in a template with 

```html
<require from="path/to/awesome-svg"></require>
```   

or globalize this resource in the framework `configure` function with

```javascript
aurelia.use.globalResources('path/to/awesome-svg');
```

which will import the `awesome-svg` component once and for all. If you do neither of these, you won't get any error and `<awesome-svg></awesome-svg>` will be treated just like any other html tag.

```html
<awesome-svg colors.bind="['#ff0000', '#00ff00', '#0000ff']"></awesome-svg>
```

Finally, it renders a set of 3 rectangles. 

![Rectangles Aurelia](./images/rectangles-aurelia.png)

Now let's do the same in Angular.

Here we need to specify both the template and the element name:

```typescript
// awesome-svg.component.ts

import {Component, Input} from '@angular/core';

@Component({
  selector: 'awesome-svg',
  templateUrl: './awesome-svg.component.html'
})
export class AwesomeSvgComponent {
  @Input() title : string;
  @Input() colors : string[] = []
}
```

The view is wgere things get a little bit complicated. First of all, Angular silently treat unknown html tags a browser does: it fires an error saying something along the lines of `'my-own-tag' is not a known element`. Same for properties you bind. 
If you had a typo somewhere in the code, it would attract attention because the app would crash. Sounds good, right? Yes and no.

If you try to do this, which is perfectly fine in terms of binding syntax:

```html
<svg>
  <rect [fill]="color"></rect>
</svg>  
```

You'll get `Can't bind to 'fill' since it isn't a known property of ':svg:rect'` and you'll need to ise `[attr.fill]="color"` syntax. 
Also, [it's required](https://github.com/angular/angular/issues/7216) to specify namespace in child-elements inside `<svg/>`: `<svg:rect>` to let Angular know this should not be treated as HTML.  

```html
<!-- awesome-svg.component.html-->

<h1>{{ title }}</h1>
<svg>
  <rect
    *ngFor="let color of colors; let i = index"
    [attr.fill]="color"
    [attr.x]="i * 100" y="0"
    width="50" height="50"
  ></rect>
</svg>

```
 
In order to use the component, you should import it in module config:
```typescript
@NgModule({
  declarations: [ AwesomeSvgComponent ]
  //...
})
``` 

Now the component may be used across this module:

```haml
<awesome-svg [colors]="['#ff0000', '#00ff00', '#0000ff']" title="Rectangles"></awesome-svg>
```

![Rectangles Angular](./images/rectangles-angular.png)
   
   
   
   
   
   
   
   
   
### We can do better

There's not so much logic in the custom element, is it? Aurelia lets us create template-only custom elements:

```html
<template bindable="colors, title">
  <h1>${title}</h1>
  <svg>
    <rect repeat.for="color of colors" 
          fill.bind="color" 
          x.bind="$index * 100" y="0" 
          width="50" height="50"></rect>
  </svg>
</template>
```

The only difference in usage is that when importing either in `configure` or `<require/>` you should put `.html` at the end:

```haml
<require from="awesome-svg.html"></require>
```

The custom element will named with respect to the file name. 

This is a really useful feature in real-life especially with custom templates and templates parts as it'll be described below.

### SVGa and child components

Suppose now we want the `<rect ...>` to be a custom component having it's own logic. It's not a problem with Angular that renders components in what matches `selector`, i.e.:

```typescript
@Component({
	selector: 'g[custom-rect]',
  ...
})
```

would render custom element to `<g custom-rect></div>` which is extremely handy.
 
Aurelia has custom attributes but they do not serve this purpose. In Aurelia, you can use the `@containerless` annotation on the custom rect element. `containerless` can also be used with custom templates without controller as shown in previous section and `<compose>` which basically renders stuff into DOM. 
With `@containerless` annotation or `containerless` attribute as demonstrated below: 
 
```html
<svg>
  <custom-rect containerless></custom-rect>
</svg>  
``` 

The output would not contain the custom element tag, i.e. instead of 
 
```html
<svg>
  <custom-rect>
    <rect ...></rect>
  </custom-rect>
</svg>  
``` 

We will get:

 
```html
<svg>
  <rect ...></rect>
</svg>  
``` 











## Services

Here Aurelia and Angular are very similar. Suppose we need NumberOperator which depends on NumberGenerator.

Here's how you define it in Aurelia:

```javascript
import {inject} from 'aurelia-framework';
import {NumberGenerator} from './number-generator'

export class NumberGenerator {
  getNumber(){
    return 42;
  }
}

@inject(NumberGenerator)
export class NumberOperator {
  constructor(numberGenerator){
    this.numberGenerator = numberGenerator;
    this.counter = 0;
  }

  getNumber(){
    return this.numberGenerator.getNumber() + this.counter++;
  }
}

```

Now, in a component, we inject in the same way:

```javascript
import {inject} from 'aurelia-framework';
import {NumberOperator} from './_services/number-operator';

@inject(NumberOperator)
export class SomeCustomElement {
  constructor(numberOperator){
    this.numberOperator = numberOperator;
    //this.numberOperator.getNumber();
  }
}
```

Basically, any class is a service. 

If what you need is a factory or just a new instance:
 
```javascript
import { Factory, NewInstance } from 'aurelia-framework';


@inject(SomeService)
export class Stuff {
  constructor(someService, config){
    this.someService = someService;
  }
}

@inject(Factory.of(Stuff), NewInstance.of(AnotherService))
export class SomethingUsingStuff {
  constructor(stuffFactory, anotherService){
    this.stuff = stuffFactory(config);
    this.anotherServiceNewInstance = anotherService;
  }
}

``` 

The DI is fully extensible, so you can write your own Resolvers. Factory and NewInstance are just a couple popular ones provided out of the box.
Now Angular:

```typescript
import { Injectable } from '@angular/core';
import { NumberGenerator } from './number-generator';

@Injectable()
export class NumberGenerator {
  getNumber(){
    return 42;
  }
}

@Injectable()
export class NumberOperator {

  counter : number = 0;

  constructor(@Inject(NumberGenerator) private numberGenerator) { }

  getNumber(){
    return this.numberGenerator.getNumber() + this.counter++;
  }
}


```

The `@Injectable` is required here. To actually inject a service, you could specify the service in `boostrap(AppComponent, [NumberGenerator, NumberOperator])` call which is not recommended or in the list of providers in component config or entire module config:

```typescript
@Component({
  //...
  providers: [NumberOperator, NumberGenerator]
})
```

Note that you need to specify both `NumberOperator` that we need and `NumberGenerator` which `NumberOperator` is dependant on.

The resulting component would look like that: 

```typescript
@Component({
  //...
  providers: [NumberOperator, NumberGenerator],
})
export class SomeComponent {
  constructor(@Inject(NumberOperator) public service){
    //service.getNumber();
  }
}
```

In Angular you can create factories with `provide` which is also used for aliasing services in order to prevent name collisions. More on that can 

So, creating a factory would look like that in Angular:

```typescript
let stuffFactory = (someService: SomeService) => {
    return new Stuff(someService);
}

@Component({
  //...
  providers: [provide(Stuff, {useFactory: stuffFactory, deps: [SomeService]})]
})
```

 
 
 
 
 
 
 
## Thansclusion
   
Angular 1 had `transclude`. Let's see what it's descendants have to offer.
   
In Angular it's called "content projection"  you can expect them work the same way `ng-transclude` did, i.e., speaking in Angular 1 terms, the slot is using parent scope. 

```typescript
@Component({
  selector: 'child',
  template: `Transcluded: <ng-content></ng-content>`
})
export class MyComponent {}
```

Use it with:

```html
<child-component>Hello from Translusion Component</child-component>
```

And we'll get the exact `Transcluded: Yes` HTML rendered in `child-component`. 

For multi-slot transclusion Angular has selectors with which you can use in the same manner as you would for `@Component` config:

```html

<!-- child.component.html -->
<h4>Slot 1:</h4>
<ng-content select=".class-selector"></ng-content>

<h4>Slot 2:</h4>
<ng-content select="[attr-selector]"></ng-content>


<!-- parent.component.html -->

<child>
  <span class="class-selector">Hello from Translusion Component</span>
  <p class="class-selector">Hello from Translusion Component again</p>
  <span attr-selector>Hello from Translusion Component one more time</span>
</child>
```

Which would render 

```html
Class selector: <div class="class-selector">Transclude 1</div>
```

You can use select on tags but remember that the tag you put there should be known to Angular.

![Angular](./images/ng-content.png)

Remember I said Aurelia follows web standards whenever possible? 
In Aurelia it's called slots and is just a polyfill for Web Components Shadow DOM: shadow dom is not created for slots _yet_, but it follows [W3C specs](http://w3c.github.io/webcomponents/spec/shadow/). 
   
```html
<!-- child -->
<template>
  Slot: <slot></slot>
</template>

<!-- parent -->
<template>
  <child>${textValue}</child>
</template>
```

You see, as I've said, Aurelia was designed to be standard-compliant, and Angular 2 wasn't.
As a result, we can do more awesome things with Aurelia's slots like fallback content and [many other cool things](http://blog.aurelia.io/2016/05/23/aurelia-shadow-dom-v1-slots-prerelease/). Trying to use fallback content fails with `<ng-content> element cannot have content` in Angular.
   
```html
<!-- child -->
<template>
  Slot A: <slot name="slot-a"></slot> <br />
  Slot B: <slot name="slot-b"></slot>
  Slot C: <slot name="slot-c">Fallback Content</slot>
</template>

<!-- parent -->
<template>
  <child>
    <div slot="slot-a">A value</div>
    <div slot="slot-b">B value</div>
  </child>
</template>
```

Both Angular and Aurelia would render all occurrences of when matches the slot name or selector.

![Aurelia](./images/slots.png)

In both Aurelia and Angular you can also compile template parts and render components dynamically using `<compose>` with `view-model` in Aurelia or `ComponentResolver` in Angular.      
 
### Shadow DOM

We mentioned Shadow DOM here. Both Aurelia and Angular support it. 

In Aurelia we have `@useShadowDOM` decorator and you're ready to go:

```javascript
import {useShadowDOM} from 'aurelia-framework';

@useShadowDOM()
export class YetAnotherCustomElement {}

```

In Angular same can be done with `ViewEncapsulation.Native` strategies:

```typescript
import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  //...
  encapsulation: ViewEncapsulation.Native,
})
export class YetAnotherComponent {}

```

Remember to check if your browser [supports](http://caniuse.com/#feat=shadowdom) shadow DOM.

## Server-side rendering

It's 2017, and server-side rendering is very trendy. You can render Angular on the backend with [Angular Universal](https://github.com/angular/universal). Aurelia will have this in 2017 as stated in [team's new year resolutions](http://blog.aurelia.io/2017/01/02/aurelia-2017-resolutions/). In fact, there's [a runnable demo](https://github.com/aurelia/skeleton-server-render/tree/develop) in Aurelia's repo. 
In addition to that, Aurelia has had Progressive Enhancement capabilities for over a year, which is something that Angular can't do because of its non-standard HTML syntax.


## Note on standard compliance

Angular is case-sensitive when it comes to html, i.e. in our svg example we can't deviate character-case to something like `<Awesome-svg/>` while Aurelia would treat that as very same `<awesome-svg>` tag. 
This means that angular can't rely on browser's HTML parser, so they created their own. Here is a [good article](http://eisenbergeffect.bluespire.com/on-angular-2-and-html/) on the topic.


## Size and performance

**Disclaimer**: dbmonster benchmark (default configs, optimized implementation) was used. It definitely does not show the full picture, but it gived us some idea.

Neither Aurelia nor Angular have a Virtual DOM implementation, both of them outperform React.

According to dbmonster benchmarks [Aurelia](http://jdanyow.github.io/aurelia-dbmonster/) and [Angular](http://mathieuancelin.github.io/js-repaint-perfs/angular2/opt.html) show similar results of approximately 100 re-renders per second on default configs (tested on MacBook Pro, what else would a frontend developer use in 2017 anyways?). It's worth noticing that on alpha stage Angular showed something around half of that result. Anyways, both Aurelia and Angular 5 times outperform Angular 1 and are 40% ahead of React.

Speaking of size, Angular is roughly twice as fat as Aurelia, but guys at Google are working on that issue.


## What to expect

As of 2017, Aurelia team [plans](http://blog.aurelia.io/2017/01/02/aurelia-2017-resolutions/) to release Aurelia UX to oppose something to Angular Material, provide more integrations and tools, as well as Server-side rendering which has been on the roadmap for a ver long while.  

Angular's roadmap includes releasing Angular 4 (yes, there's no Angular 3, and now the version number should be dropped when talking about Angular since major releases are planned every 6 months) and plans to make it smaller and more lightweight meanwhile improving developer experience. If you look at the path Angular did from alpha to current version you'll see that it wasn't always coherent. It was changing really much on alpha stage, renaming attributes from build to build, etc. Now the Angular team promises that breaking changes won't be easy-to-migrate. 


## Final words

Both Angular and Aurelia are good. They are very similar yet different, but definitely good. It's not a question of "does this framework allow me to..." because the answer is simply yes. Angular and Aurelia can and should be compared: they give roughly the same functionality, maybe except some specific syntax sugar or small things, but they follow different philosophies, styles and completely different approaches to web standards. Choosing one over another is a matter of personal taste and priorities.   

